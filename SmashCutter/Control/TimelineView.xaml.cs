﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SmashCutter.Control
{
    /// <summary>
    /// Interaction logic for TimelineView.xaml
    /// </summary>
    public partial class TimelineView : UserControl
    {
        public event RoutedEventHandler MediaLoaded;
        public event RoutedEventHandler MediaFailed;

        public double FrameRate { get; set; } = 60;

        public int SkipDistance { get; set; } = 1000;

        public bool IsPlaying { get; set; }

        public double MaxTime { get; set; }

        private double startTime;
        private double currentTime;

        public double CurrentTime
        {
            get
            {
                return this.currentTime;
            }
            set
            {
                this.currentTime = value;
                double left = this.currentTime / this.MaxTime * this.ActualWidth;
                SetMarginWidth(this.Trackhead, left);
                SetMarginWidth(this.Trackline, left);
            }
        }

        public double SegmentStart
        {
            get
            {
                if (this.LoadedObject is Workspace.Timeline)
                {
                    double clipTime;
                    double clipStart;
                    var clip = this.getClipAtTime(this.currentTime, out clipTime, out clipStart);
                    return clipStart;
                }
                else
                {
                    var lastBreak = this.breakLines.Where(x => x.Time <= this.currentTime);
                    if (lastBreak.Count() > 0)
                        return lastBreak.Last().Time;
                    return 0;
                }
            }
        }

        public double SegmentEnd
        {
            get
            {
                if (this.LoadedObject is Workspace.Timeline)
                {
                    double clipTime;
                    double clipStart;
                    var clip = this.getClipAtTime(this.currentTime, out clipTime, out clipStart);
                    return clipStart + (clip.EndTime - clip.StartTime);
                }
                else
                {
                    var lastBreak = this.breakLines.Where(x => x.Time >= this.currentTime);
                    if (lastBreak.Count() > 0)
                        return lastBreak.First().Time;
                    return this.MaxTime;
                }
            }
        }

        public Workspace.Clip CurrentClip
        {
            get
            {
                double dump1, dump2;
                return this.getClipAtTime(this.currentTime, out dump1, out dump2);
            }
        }

        private IEnumerable<Workspace.Source> sourceClips;
        public object LoadedObject { get; private set; }

        public bool HasLoadedVideo
        {
            get
            {
                return this.MediaElement.IsLoaded && this.MaxTime > 0;
            }
        }

        private static void SetMarginWidth(FrameworkElement element, double left)
        {
            var margin = element.Margin;
            margin.Left = left;
            element.Margin = margin;
        }

        private DispatcherTimer timer;

        private IList<BreakLine> breakLines;

        public TimelineView()
        {
            InitializeComponent();
            this.breakLines = new List<BreakLine>();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.MediaElement.Loaded += MediaElement_Loaded;
            this.MediaElement.MediaFailed += MediaElement_MediaFailed;
            this.MediaElement.MediaOpened += MediaElement_MediaOpened;
            this.MediaElement.MediaEnded += MediaElement_MediaEnded;
            this.MediaElement.LoadedBehavior = MediaState.Manual;
            this.MediaElement.ScrubbingEnabled = true;

            this.timer = new DispatcherTimer();
            this.timer.Interval = new TimeSpan(0, 0, 0, 0, 16); //slightly faster than 60fps
            this.timer.Tick += Timer_Tick;

            var window = Window.GetWindow(this);
            if (window != null)
            {
                window.KeyDown += UserControl_KeyDown;
                window.KeyUp += UserControl_KeyUp;
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            window.KeyDown -= UserControl_KeyDown;
            window.KeyUp -= UserControl_KeyUp;
        }

        public void Load(string filename)
        {
            this.MediaElement.Source = null;
            this.startTime = 0;
            this.Slideshow.Fill = Brushes.Black;
            this.LoadedObject = filename;
            this.ClearBreaks();
            this.MediaElement.Source = new Uri(filename);
        }

        public void Load(Workspace.Source source)
        {
            this.MediaElement.Source = null;
            this.startTime = 0;
            this.Slideshow.Fill = Brushes.Black;
            this.LoadedObject = source;
            this.ClearBreaks();
            this.MediaElement.Source = new Uri(source.FileName);
            this.SetBreaks(source.Breaks);
        }

        public void Load(Workspace.Timeline timeline, IEnumerable<Workspace.Source> sources)
        {
            this.MediaElement.Source = null;
            this.LoadedObject = timeline;
            this.Slideshow.Fill = Brushes.DarkGray;
            this.ClearBreaks();
            this.sourceClips = sources;
            if (timeline.Clips.Count > 0)
            {
                this.startTime = timeline.Clips.First().StartTime;
                this.MaxTime = timeline.Clips.Select(x => x.EndTime - x.StartTime).Sum();
                this.MediaElement.Source = new Uri(sources.Where(x => x.Name.Equals(timeline.Clips.First().SourceName)).First().FileName);
                this.ClearBreaks();
                float start = 0;
                foreach (var clip in timeline.Clips)
                {
                    this.breakLines.Add(new BreakLine(start, this.Slideshow.ActualHeight, true, this.Grid.Children));
                    start += clip.EndTime - clip.StartTime;
                }
            }
        }

        public void Load(string filename, IEnumerable<Workspace.Breakpoint> breaks)
        {
            this.MediaElement.Source = null;
            this.startTime = 0;
            this.Slideshow.Fill = Brushes.Black;
            this.ClearBreaks();
            foreach (var b in breaks)
                this.breakLines.Add(new BreakLine(b.Position, this.Slideshow.ActualHeight, b.IsAuto, this.Grid.Children));
            this.breakLines.OrderBy(x => x.Time);
            this.MediaElement.Source = new Uri(filename);
        }

        public void AddBreak(Workspace.Breakpoint breakpoint)
        {
            this.breakLines.Add(new BreakLine(breakpoint.Position, this.Slideshow.ActualHeight, breakpoint.IsAuto, this.Grid.Children));
            this.breakLines.OrderBy(x => x.Time);
            this.DrawBreaks();
        }

        private void preSetBreaks(IEnumerable<Workspace.Breakpoint> breaks)
        {
            this.ClearBreaks();
            foreach (var b in breaks)
                this.breakLines.Add(new BreakLine(b.Position, this.Slideshow.ActualHeight, b.IsAuto, this.Grid.Children));
            this.breakLines = this.breakLines.OrderBy(x => x.Time).ToList();
        }

        public void SetBreaks(IEnumerable<Workspace.Breakpoint> breaks)
        {
            this.preSetBreaks(breaks);
            this.DrawBreaks();
        }

        public void Unload()
        {
            this.ClearBreaks();
            this.MediaElement.Source = null;
        }

        public void Pause(bool updatePlaying = true)
        {
            if (this.MediaElement.IsLoaded)
            {
                this.MediaElement.Pause();
                this.timer.IsEnabled = false;
                if (updatePlaying)
                {
                    this.IsPlaying = false;
                    this.PlayButton.Visibility = Visibility.Visible;
                    this.PauseButton.Visibility = Visibility.Collapsed;
                }
            }
        }

        public void Play()
        {
            if (this.MediaElement.IsLoaded)
            {
                var current = this.MediaElement.Position.TotalMilliseconds;
                this.MediaElement.Play();
                this.MediaElement.Position = new TimeSpan(0, 0, 0, 0, (int)current);
                this.timer.IsEnabled = true;
                this.IsPlaying = true;
                this.PlayButton.Visibility = Visibility.Collapsed;
                this.PauseButton.Visibility = Visibility.Visible;
            }
        }

        private Workspace.Clip getClipAtTime(double time, out double timeInClip, out double clipStart)
        {
            var timeline = this.LoadedObject as Workspace.Timeline;
            timeInClip = 0;
            clipStart = 0;
            foreach (var clip in timeline.Clips)
            {
                timeInClip = time - clipStart;
                var clipLength = clip.EndTime - clip.StartTime;
                if (time <= clipStart + clipLength)
                {
                    return clip;
                }
                clipStart += clipLength;
            }
            var lastClip = timeline.Clips.Last();
            if (time - clipStart > lastClip.EndTime - lastClip.StartTime)
                return null;
            return timeline.Clips.Last();
        }

        public void SkipTo(double time)
        {
            if (this.MediaElement.IsLoaded)
            {
                this.CurrentTime = time;
                if (this.LoadedObject is Workspace.Timeline)
                {
                    var timeline = this.LoadedObject as Workspace.Timeline;
                    double clipTime;
                    double clipStart;
                    var clip = this.getClipAtTime(time, out clipTime, out clipStart);
                    if (clip == null)
                    {
                        var lastClip = timeline.Clips.Last();
                        this.MediaElement.Position = new TimeSpan(0, 0, 0, 0, (int)lastClip.EndTime);
                    }
                    else
                    {
                        var currentFilename = this.sourceClips.Where(x => x.Name.Equals(clip.SourceName)).First().FileName;
                        var currentUri = new Uri(currentFilename);
                        if (!currentUri.Equals(this.MediaElement.Source))
                        {
                            this.startTime = clip.StartTime + clipTime;
                            this.MediaElement.Source = new Uri(currentFilename);
                        }
                        else
                        {
                            this.MediaElement.Position = new TimeSpan(0, 0, 0, 0, (int)(clip.StartTime + clipTime));
                        }
                    }
                }
                else
                {
                    this.MediaElement.Position = new TimeSpan(0, 0, 0, 0, (int)this.CurrentTime);
                }
            }
        }

        public void SkipAhead(double distance)
        {
            this.SkipTo(Math.Min(this.CurrentTime + distance, this.MaxTime));
        }

        public void SkipBack(double distance)
        {
            this.SkipTo(Math.Max(this.CurrentTime - distance, 0));
        }

        private void ClearBreaks()
        {
            foreach (var breakLine in this.breakLines)
                this.Grid.Children.Remove(breakLine.Line);
            this.breakLines.Clear();
        }

        private void DrawBreaks()
        {
            foreach (var breakLine in this.breakLines)
                SetMarginWidth(breakLine.Line, breakLine.Time / this.MaxTime * this.ActualWidth);
        }

        private void MediaElement_Loaded(object sender, RoutedEventArgs e)
        {
            this.Pause();
        }

        private void MediaElement_MediaFailed(object sender, RoutedEventArgs e)
        {
            if (this.MediaFailed != null)
                this.MediaFailed(sender, e);
        }

        private void MediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (!(this.LoadedObject is Workspace.Timeline))
                this.MaxTime = this.MediaElement.NaturalDuration.TimeSpan.TotalMilliseconds;
            this.MediaElement.Position = new TimeSpan(0, 0, 0, 0, (int)this.startTime);
            this.DrawBreaks();
            if (this.MediaLoaded != null)
                this.MediaLoaded(sender, e);
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            this.Pause();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.LoadedObject is Workspace.Timeline)
            {
                var timeline = this.LoadedObject as Workspace.Timeline;
                double oldClipTime = 0;
                double newClipTime = 0;
                double oldClipStart = 0;
                double newClipStart = 0;
                var oldClip = this.getClipAtTime(this.currentTime, out oldClipTime, out oldClipStart);
                if (oldClip == null)
                {
                    this.Pause();
                    var lastClip = timeline.Clips.Last();
                    this.SkipTo(lastClip.EndTime);
                    return;
                }
                var newClip = this.getClipAtTime(oldClipStart + (this.MediaElement.Position.TotalMilliseconds - oldClip.StartTime), out newClipTime, out newClipStart);
                if (newClip == null)
                {
                    this.Pause();
                    var lastClip = timeline.Clips.Last();
                    this.SkipTo(lastClip.EndTime);
                    return;
                }
                else if (oldClip == newClip)
                {
                    this.CurrentTime = newClipStart + newClipTime;
                }
                else
                {
                    this.SkipTo(newClipStart + newClipTime);
                }
            }
            else
            {
                this.CurrentTime = this.MediaElement.Position.TotalMilliseconds;
            }
        }

        private void Slideshow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Pause(false);
            this.SkipTo(e.GetPosition(this).X / this.ActualWidth * this.MaxTime);
        }

        private void Slideshow_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed || e.RightButton == MouseButtonState.Pressed)
                this.SkipTo(e.GetPosition(this).X / this.ActualWidth * this.MaxTime);
        }

        private void Slideshow_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.IsPlaying)
                this.Play();
        }

        private void Slideshow_MouseLeave(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed || e.RightButton == MouseButtonState.Pressed)
            {
                if (this.IsPlaying)
                    this.Play();
            }
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            this.Play();
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Pause();
        }

        private void SkipBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.SkipBack(this.SkipDistance);
        }

        private void SkipAheadButton_Click(object sender, RoutedEventArgs e)
        {
            this.SkipAhead(this.SkipDistance);
        }

        private bool isShiftDown;
        private bool isControlDown;

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                this.isShiftDown = true;
            }
            else if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
            {
                this.isControlDown = true;
            }
            else if (e.Key == Key.Space)
            {
                if (this.IsPlaying)
                    this.Pause();
                else
                    this.Play();
            }
            else if (e.Key == Key.Left)
            {
                this.Pause(false);
                if (this.isShiftDown)
                {
                    this.SkipBack(this.SkipDistance);
                }
                else if (this.isControlDown)
                {
                    bool done = false;
                    for (var i = this.breakLines.Count - 1; i >= 0; i--)
                    {
                        if (this.currentTime > this.breakLines[i].Time)
                        {
                            this.SkipTo(this.breakLines[i].Time);
                            done = true;
                            break;
                        }
                    }
                    if (!done)
                        this.SkipTo(0);
                }
                else
                {
                    this.SkipBack(1000 / this.FrameRate);
                }
            }
            else if (e.Key == Key.Right)
            {
                this.Pause(false);
                if (this.isShiftDown)
                {
                    this.SkipAhead(this.SkipDistance);
                }
                else if (this.isControlDown)
                {
                    bool done = false;
                    for (var i = 0; i < this.breakLines.Count; i++)
                    {
                        if (this.currentTime < this.breakLines[i].Time)
                        {
                            this.SkipTo(this.breakLines[i].Time);
                            done = true;
                            break;
                        }
                    }
                    if (!done)
                        this.SkipTo(this.MaxTime);
                }
                else
                {
                    this.SkipAhead(1000 / this.FrameRate);
                }
            }
        }

        private void UserControl_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                this.isShiftDown = false;
            }
            else if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
            {
                this.isControlDown = false;
            }
            else if (e.Key == Key.Left || e.Key == Key.Right)
            {
                if (this.IsPlaying)
                    this.Play();
            }
        }

        private void Slideshow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.MediaElement.IsLoaded && this.MaxTime > 0)
            {
                double left = this.currentTime / this.MaxTime * this.ActualWidth;
                SetMarginWidth(this.Trackhead, left);
                SetMarginWidth(this.Trackline, left);
                this.DrawBreaks();
            }
        }
    }
}
