﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SmashCutter.Control
{
    public class BreakLine
    {
        public double Time { get; set; }
        public Line Line { get; set; }
        
        public BreakLine(double time, double height, bool isAuto, UIElementCollection parent)
        {
            this.Time = time;
            this.Line = new Line();
            this.Line.X1 = 0;
            this.Line.X2 = 0;
            this.Line.Y1 = 0;
            this.Line.Y2 = height;
            if (isAuto)
                this.Line.Stroke = Brushes.CornflowerBlue;
            else
                this.Line.Stroke = Brushes.Red;
            this.Line.StrokeThickness = 1;
            this.Line.SnapsToDevicePixels = true;
            this.Line.UseLayoutRounding = true;
            Grid.SetRow(this.Line, 2);
            parent.Add(this.Line);
        }
    }
}
