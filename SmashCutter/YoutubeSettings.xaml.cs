﻿using Google.Apis.YouTube.v3.Data;
using SmashCutter.Youtube;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SmashCutter
{
    /// <summary>
    /// Interaction logic for YoutubeSettings.xaml
    /// </summary>
    public partial class YoutubeSettings : Window
    {
        public VideoCategory Category { get; set; }
        public Playlist Playlist { get; set; }

        public YoutubeSettings()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var service = YoutubeUtils.GetService(YoutubeUtils.Authorize());
            var categories = YoutubeUtils.GetCategories(service);
            foreach (var category in categories)
                this.CategoryBox.Items.Add(category);
            this.CategoryBox.SelectedItem = this.CategoryBox.Items[0];

            var playlists = YoutubeUtils.GetPlaylists(service);
            foreach (var playlist in playlists)
                this.PlaylistBox.Items.Add(playlist);
            this.PlaylistBox.SelectedItem = this.PlaylistBox.Items[0];
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Category = this.CategoryBox.SelectedItem as VideoCategory;
            this.Playlist = this.PlaylistBox.SelectedItem as Playlist;
            this.DialogResult = true;
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.DialogResult == null)
                this.DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Category = this.CategoryBox.SelectedItem as VideoCategory;
                this.Playlist = this.PlaylistBox.SelectedItem as Playlist;
                this.DialogResult = true;
                this.Close();
            }
            else if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
                this.Close();
            }
        }
    }
}
