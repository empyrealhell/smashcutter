﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Microsoft.Win32;
using Newtonsoft.Json;
using SmashCutter.Control;
using SmashCutter.ffmpeg;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SmashCutter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Workspace.Workspace activeWorkspace;
        private bool hasUpdates;
        private string workspaceFile;
        private string appTitle;
        private IList<IQueueItem> queue;

        public MainWindow()
        {
            InitializeComponent();
            this.activeWorkspace = new Workspace.Workspace();
            this.appTitle = this.Title;
            this.queue = new List<IQueueItem>();
        }

        private void updateTitle()
        {
            string newTitle = this.appTitle;
            if (string.IsNullOrEmpty(this.workspaceFile))
                newTitle += " - Untitled Workspace";
            else
                newTitle += " - " + System.IO.Path.GetFileNameWithoutExtension(this.workspaceFile);
            if (this.hasUpdates)
                newTitle += "*";
            this.Title = newTitle;
        }

        private void setDirty(bool dirty)
        {
            this.hasUpdates = dirty;
            this.updateTitle();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.updateTitle();
        }

        private void NewWorkspace(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.hasUpdates)
            {
                var result = MessageBox.Show("The current workspace has unsaved data, would you like to save first?", "Save Unsaved Changes?", MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
                if (result == MessageBoxResult.Yes)
                {
                    this.SaveWorkspace(sender, e);
                    if (this.hasUpdates)    //user canceled the save
                        return;
                }
                else if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }
            this.SourceClipList.Items.Clear();
            this.TimelineList.Items.Clear();
            this.workspaceFile = null;
            this.setDirty(false);
        }

        private void OpenWorkspace(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.hasUpdates)
            {
                var result = MessageBox.Show("The current workspace has unsaved data, would you like to save first?", "Save Unsaved Changes?", MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
                if (result == MessageBoxResult.Yes)
                {
                    this.SaveWorkspace(sender, e);
                    if (this.hasUpdates)    //user canceled the save
                        return;
                }
                else if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = "*.vws";
            ofd.Filter = "Video Workspace (*.vws)|*.vws";

            if (ofd.ShowDialog() == true)
            {
                if (File.Exists(ofd.FileName))
                {
                    try
                    {
                        this.activeWorkspace = JsonConvert.DeserializeObject<Workspace.Workspace>(File.ReadAllText(ofd.FileName));
                        this.SourceClipList.Items.Clear();
                        foreach (var source in this.activeWorkspace.Sources)
                            this.SourceClipList.Items.Add(source);
                        this.TimelineList.Items.Clear();
                        foreach (var timeline in this.activeWorkspace.Timelines)
                            this.TimelineList.Items.Add(timeline);
                        this.workspaceFile = ofd.FileName;
                        this.setDirty(false);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to load file: " + ex.Message, "Load Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void SaveWorkspace(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.hasUpdates)
            {
                if (string.IsNullOrEmpty(this.workspaceFile))
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.DefaultExt = "*.vws";
                    sfd.Filter = "Video Workspace (*.vws)|*.vws";

                    if (sfd.ShowDialog() == true)
                    {
                        if (File.Exists(sfd.FileName))
                        {
                            var result = MessageBox.Show("Workspace file \"" + sfd.FileName + "\" already exists. Are you sure you want to overwrite it?", "Confirm File Overwrite", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                            if (result == MessageBoxResult.Yes)
                                this.workspaceFile = sfd.FileName;
                        }
                        else
                        {
                            this.workspaceFile = sfd.FileName;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(this.workspaceFile))
                {
                    string json = JsonConvert.SerializeObject(this.activeWorkspace);
                    File.WriteAllText(this.workspaceFile, json);
                    this.setDirty(false);
                }
            }
        }

        private void NewTimeline(object sender, ExecutedRoutedEventArgs e)
        {
            InputBox ib = new InputBox("Enter the name for your new timeline", "");
            var result = ib.ShowDialog();
            if (result == true && !string.IsNullOrWhiteSpace(ib.Value))
            {
                var newTimeline = new Workspace.Timeline(ib.Value);
                this.activeWorkspace.Timelines.Add(newTimeline);
                this.TimelineList.Items.Add(newTimeline);
                this.setDirty(true);
            }
        }

        private void RenameTimeline(object sender, ExecutedRoutedEventArgs e)
        {
            var timeline = this.TimelineList.SelectedItem as Workspace.Timeline;
            if (timeline != null)
            {
                InputBox ib = new InputBox("Enter a new name for this timeline", timeline.Name);
                var result = ib.ShowDialog();
                if (result == true && !string.IsNullOrWhiteSpace(ib.Value))
                {
                    timeline.Name = ib.Value;
                    this.TimelineList.Items.Refresh();
                    this.setDirty(true);
                }
            }
        }

        private void DeleteTimeline(object sender, ExecutedRoutedEventArgs e)
        {
            var timeline = this.TimelineList.SelectedItem as Workspace.Timeline;
            if (timeline != null)
            {
                var result = MessageBox.Show("The timeline \"" + timeline.Name + "\" will be permanently deleted. This will not affect any videos uploaded or exported from it. Are you sure you want to delete it?", "Confirm Timeline Delete", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
                if (result == MessageBoxResult.Yes)
                {
                    this.activeWorkspace.Timelines.Remove(timeline);
                    this.TimelineList.Items.Remove(timeline);
                    this.setDirty(true);
                }
            }
        }

        private void AddBreakpoint(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.VideoPlayer.HasLoadedVideo)
            {
                if (this.VideoPlayer.LoadedObject is Workspace.Source)
                {
                    var source = this.VideoPlayer.LoadedObject as Workspace.Source;
                    var breakpoint = new Workspace.Breakpoint((long)this.VideoPlayer.CurrentTime, false);
                    source.Breaks.Add(breakpoint);
                    this.VideoPlayer.SetBreaks(source.Breaks);
                    this.setDirty(true);
                }
            }
        }

        private void AddToTimeline(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.VideoPlayer.HasLoadedVideo)
            {
                if (this.VideoPlayer.LoadedObject is Workspace.Source)
                {
                    var source = this.VideoPlayer.LoadedObject as Workspace.Source;
                    if (this.TimelineList.SelectedItem != null)
                    {
                        var timeline = this.TimelineList.SelectedItem as Workspace.Timeline;
                        var clip = new Workspace.Clip(source.Name, (long)this.VideoPlayer.SegmentStart, (long)this.VideoPlayer.SegmentEnd);
                        timeline.Clips.Add(clip);
                        this.setDirty(true);
                        this.TimelineList.Items.Refresh();
                    }
                }
            }
        }

        private void RemoveClip(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.VideoPlayer.HasLoadedVideo)
            {
                if (this.VideoPlayer.LoadedObject is Workspace.Timeline)
                {
                    var timeline = this.VideoPlayer.LoadedObject as Workspace.Timeline;
                    var clip = this.VideoPlayer.CurrentClip;
                    if (clip != null)
                    {
                        timeline.Clips.Remove(clip);
                        this.VideoPlayer.Load(timeline, this.activeWorkspace.Sources);
                        this.setDirty(true);
                    }
                }
            }
        }

        private void ShiftLeft(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.VideoPlayer.HasLoadedVideo)
            {
                if (this.VideoPlayer.LoadedObject is Workspace.Timeline)
                {
                    var timeline = this.VideoPlayer.LoadedObject as Workspace.Timeline;
                    var clip = this.VideoPlayer.CurrentClip;
                    if (clip != null)
                    {
                        int index = timeline.Clips.IndexOf(clip);
                        if (index > 0)
                        {
                            var swapClip = timeline.Clips[index - 1];
                            timeline.Clips[index - 1] = clip;
                            timeline.Clips[index] = swapClip;
                            this.VideoPlayer.Load(timeline, this.activeWorkspace.Sources);
                            this.setDirty(true);
                        }
                    }
                }
            }
        }

        private void ShiftRight(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.VideoPlayer.HasLoadedVideo)
            {
                if (this.VideoPlayer.LoadedObject is Workspace.Timeline)
                {
                    var timeline = this.VideoPlayer.LoadedObject as Workspace.Timeline;
                    var clip = this.VideoPlayer.CurrentClip;
                    if (clip != null)
                    {
                        int index = timeline.Clips.IndexOf(clip);
                        if (index < timeline.Clips.Count - 1)
                        {
                            var swapClip = timeline.Clips[index + 1];
                            timeline.Clips[index + 1] = clip;
                            timeline.Clips[index] = swapClip;
                            this.VideoPlayer.Load(timeline, this.activeWorkspace.Sources);
                            this.setDirty(true);
                        }
                    }
                }
            }
        }

        private void AddSource(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".mp4";
            ofd.Filter = "MP4 file (*.mp4)|*.mp4";

            if (ofd.ShowDialog() == true)
            {
                string videofile = ofd.FileName;
                var sourceClip = new Workspace.Source(videofile);
                this.activeWorkspace.Sources.Add(sourceClip);
                this.SourceClipList.Items.Add(sourceClip);
                this.SourceClipList.SelectedItem = sourceClip;
                this.setDirty(true);
            }
        }

        private void AnalyzeSource(object sender, ExecutedRoutedEventArgs e)
        {
            var source = this.SourceClipList.SelectedItem as Workspace.Source;
            if (source != null)
            {
                var result = MessageBox.Show("This process takes a long time, possibly up to the length of the video being processed. The workspace will be automatically saved upon completion. Are you sure you want to continue?", "Confirm Analysis", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
                if (result == MessageBoxResult.Yes)
                {
                    source.IsPending = true;
                    IProgress<ProgressData> progress = new Progress<ProgressData>(x =>
                    {
                        source.Message = x.Message;
                        source.Progress = (float)Math.Round((float)x.CurrentValue / (float)x.MaxValue * 100f, 2);
                        this.SourceClipList.Items.Refresh();
                    });
                    var processTask = Task.Factory.StartNew(() =>
                    {
                        var breaks = VideoUtilities.BlackDetect(source.FileName, progress).ToList();
                        foreach (var br in breaks)
                            source.Breaks.Add(new Workspace.Breakpoint(br, true));
                    });
                    processTask.ContinueWith(x =>
                    {
                        source.IsPending = false;
                        source.Progress = 0;
                        this.hasUpdates = true;
                        this.SaveWorkspace(null, null);
                        if (this.SourceClipList.SelectedItem == source)
                            this.VideoPlayer.SetBreaks(source.Breaks);
                        this.SourceClipList.Items.Refresh();
                    }, TaskScheduler.FromCurrentSynchronizationContext());
                }
            }
        }

        private void ResetSource(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Not Implemented");
        }

        private void RemoveSource(object sender, ExecutedRoutedEventArgs e)
        {
            var source = this.SourceClipList.SelectedItem as Workspace.Source;
            if (source != null)
            {
                var result = MessageBox.Show("Are you sure you want to remove " + source.Name + " as a source clip? Any timelines with references to this source will have those references removed. This action cannot be undone.", "Confirm Source Removal", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
                if (result == MessageBoxResult.Yes)
                {
                    this.activeWorkspace.Sources.Remove(source);
                    this.SourceClipList.Items.Remove(source);
                    this.setDirty(true);
                }
            }
        }

        private void AddQueueItem(IQueueItem item)
        {
            this.queue.Add(item);
            if (this.queue.Count == 1)
                this.ExecuteNextQueueItem();
        }

        private void ExecuteNextQueueItem()
        {
            if (this.queue.Count > 0)
            {
                Utils.SetThreadContinuous();
                IQueueItem item = this.queue[0];
                var task = Task.Factory.StartNew(item.Execute, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
                task.ContinueWith(x =>
                {
                    item.ContinuationTask(x);
                    this.queue.RemoveAt(0);
                    this.ExecuteNextQueueItem();
                }, CancellationToken.None, TaskContinuationOptions.HideScheduler, TaskScheduler.FromCurrentSynchronizationContext());
            }
            else
            {
                Utils.SetThreadNormal();
            }
        }

        private void ExportToFile(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.TimelineList.SelectedItems.Count > 1)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.DefaultExt = "*.*";
                sfd.Filter = "Folder|*.*";

                if (sfd.ShowDialog() == true)
                {
                    string pathName = System.IO.Path.GetDirectoryName(sfd.FileName);
                    foreach (var timelineItem in this.TimelineList.SelectedItems)
                    {
                        var timeline = timelineItem as Workspace.Timeline;
                        timeline.IsPending = true;
                        IProgress<ProgressData> progress = new Progress<ProgressData>(x =>
                        {
                            timeline.Message = x.Message;
                            timeline.Progress = (float)Math.Round((float)x.CurrentValue / (float)x.MaxValue * 100f, 2);
                            this.TimelineList.Items.Refresh();
                        });
                        QueueExport item = new QueueExport(timeline, this.activeWorkspace, System.IO.Path.Combine(pathName, timeline.Name + ".mp4"), progress);
                        item.ContinuationTask = (x) =>
                        {
                            timeline.Progress = 0;
                            timeline.IsPending = false;
                            this.TimelineList.Items.Refresh();
                        };
                        this.AddQueueItem(item);
                    }
                }
            }
            else
            {
                Workspace.Timeline timeline = this.TimelineList.SelectedItem as Workspace.Timeline;
                if (timeline != null)
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.DefaultExt = "*.mp4";
                    sfd.Filter = "MP4 Video (*.mp4)|*.mp4";
                    sfd.FileName = timeline.Name;

                    if (sfd.ShowDialog() == true)
                    {
                        timeline.IsPending = true;
                        IProgress<ProgressData> progress = new Progress<ProgressData>(x =>
                        {
                            timeline.Message = x.Message;
                            timeline.Progress = (float)Math.Round((float)x.CurrentValue / (float)x.MaxValue * 100f, 2);
                            this.TimelineList.Items.Refresh();
                        });
                        QueueExport item = new QueueExport(timeline, this.activeWorkspace, sfd.FileName, progress);
                        item.ContinuationTask = (x) =>
                        {
                            timeline.Progress = 0;
                            timeline.IsPending = false;
                            this.TimelineList.Items.Refresh();
                        };
                        this.AddQueueItem(item);
                    }
                }
            }
        }

        private void Publish(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.TimelineList.SelectedItems.Count > 1)
            {
                YoutubeSettings ys = new YoutubeSettings();

                if (ys.ShowDialog() == true)
                {
                    foreach (var timelineItem in this.TimelineList.SelectedItems)
                    {
                        var timeline = timelineItem as Workspace.Timeline;
                        timeline.IsPending = true;
                        IProgress<ProgressData> progress = new Progress<ProgressData>(x =>
                        {
                            timeline.Message = x.Message;
                            timeline.Progress = (float)Math.Round((float)x.CurrentValue / (float)x.MaxValue * 100f, 2);
                            this.TimelineList.Items.Refresh();
                        });
                        QueuePublish item = new QueuePublish(this.activeWorkspace, timeline, ys.Category.Id, ys.Playlist, progress);
                        item.ContinuationTask = (x) =>
                        {
                            timeline.Progress = 0;
                            timeline.IsPending = false;
                            this.TimelineList.Items.Refresh();
                        };
                        this.AddQueueItem(item);
                    }
                }
            }
            else
            {
                Workspace.Timeline timeline = this.TimelineList.SelectedItem as Workspace.Timeline;
                if (timeline != null)
                {
                    YoutubeSettings ys = new YoutubeSettings();

                    if (ys.ShowDialog() == true)
                    {
                        timeline.IsPending = true;
                        IProgress<ProgressData> progress = new Progress<ProgressData>(x =>
                        {
                            timeline.Message = x.Message;
                            timeline.Progress = (float)Math.Round((float)x.CurrentValue / (float)x.MaxValue * 100f, 2);
                            this.TimelineList.Items.Refresh();
                        });
                        QueuePublish item = new QueuePublish(this.activeWorkspace, timeline, ys.Category.Id, ys.Playlist, progress);
                        item.ContinuationTask = (x) =>
                        {
                            timeline.Progress = 0;
                            timeline.IsPending = false;
                            this.TimelineList.Items.Refresh();
                        };
                        this.AddQueueItem(item);
                    }
                }
            }
        }

        private void SourceClipList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Workspace.Source source = this.SourceClipList.SelectedItem as Workspace.Source;
            if (source != null)
                this.VideoPlayer.Load(source);
            else
                this.VideoPlayer.Unload();
        }

        private void TimelineList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Workspace.Timeline timeline = this.TimelineList.SelectedItem as Workspace.Timeline;
            if (timeline != null)
                this.VideoPlayer.Load(timeline, this.activeWorkspace.Sources);
            else
                this.VideoPlayer.Unload();
        }

        private void TimelineList_GotFocus(object sender, RoutedEventArgs e)
        {
            Workspace.Timeline timeline = this.TimelineList.SelectedItem as Workspace.Timeline;
            if (timeline != null)
                this.VideoPlayer.Load(timeline, this.activeWorkspace.Sources);
        }

        private void SourceClipList_GotFocus(object sender, RoutedEventArgs e)
        {
            Workspace.Source source = this.SourceClipList.SelectedItem as Workspace.Source;
            if (source != null)
                this.VideoPlayer.Load(source);
        }
    }
}
