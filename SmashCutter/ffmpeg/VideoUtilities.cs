﻿using SmashCutter.Workspace;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SmashCutter.ffmpeg
{
    public class VideoUtilities
    {
        private static Regex update = new Regex(@"^\s*frame=\s*\d+\s+fps=\s*\d+(?:\.\d+)?\s+q=\-?\d+\.\d+\s+size=(?:(?:N/A)|(?:\s*\d+\w{2}))\s+time=(\d{2}):(\d{2}):(\d{2})\.(\d{2})\s+bitrate=(?:(?:N/A)|(?:\s*\d+(?:\.\d+)?\w{5}/s))\s+speed=\s*\d+\.\d+x\s*$");
        private static Regex blackDetect = new Regex(@"^\s*\[blackdetect @ [0-9a-fA-F]+\]\s+black_start:(\d+\.\d+)\s+black_end:(\d+\.\d+)\s+black_duration:(\d+\.\d+)\s*$");
        private static Regex duration = new Regex(@"^\s*Duration:\s+(\d{2}):(\d{2}):(\d{2})\.(\d{2})\,\s+start:\s+\d+(?:\.\d+)?\,\s+bitrate:\s+\d+\s+\w+/s\s*$");

        private static Process StartFFMpeg(string arguments)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = Path.Combine(Environment.CurrentDirectory, "ffmpeg", "ffmpeg.exe");
            proc.StartInfo.Arguments = arguments;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.Start();
            return proc;
        }

        public static IEnumerable<long> BlackDetect(string videofile, IProgress<ProgressData> updateCallback)
        {
            List<long> detections = new List<long>();
            var proc = VideoUtilities.StartFFMpeg("-i \"" + videofile + "\" -vf \"blackdetect=d=0.2:pix_th=0.00\" -an -f null 2");
            ProgressData data = new ProgressData();
            data.Message = "Scanning";
            while (!proc.StandardError.EndOfStream)
            {
                var line = proc.StandardError.ReadLine();
                if (data.MaxValue == 0)
                {
                    Match m = duration.Match(line);
                    if (m.Success)
                    {
                        int hours = int.Parse(m.Groups[1].Value);
                        int minutes = int.Parse(m.Groups[2].Value);
                        int seconds = int.Parse(m.Groups[3].Value);
                        int milliseconds = int.Parse(m.Groups[4].Value);
                        data.MaxValue = milliseconds + seconds * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
                    }
                }
                else
                {
                    Match m = update.Match(line);
                    if (m.Success)
                    {
                        int hours = int.Parse(m.Groups[1].Value);
                        int minutes = int.Parse(m.Groups[2].Value);
                        int seconds = int.Parse(m.Groups[3].Value);
                        int milliseconds = int.Parse(m.Groups[4].Value);
                        long msProcessed = milliseconds + seconds * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
                        data.CurrentValue = msProcessed;
                        updateCallback.Report(data);
                    }
                    m = blackDetect.Match(line);
                    if (m.Success)
                    {
                        detections.Add((long)(double.Parse(m.Groups[1].Value) * 1000));
                        detections.Add((long)(double.Parse(m.Groups[2].Value) * 1000));
                    }
                }
            }
            proc.WaitForExit();
            return detections;
        }

        public static void WriteTimeline(Workspace.Workspace workspace, Timeline timeline, string exportFile, IProgress<ProgressData> progress)
        {
            ProgressData data = new ProgressData();
            data.Message = "Encoding Video";
            data.MaxValue = timeline.Length * 2;
            List<string> files = new List<string>();
            long totalProcessed = 0;
            foreach (var clip in timeline.Clips)
            {
                string filename = workspace.Sources.Where(x => x.Name.Equals(clip.SourceName)).Select(x => x.FileName).First();
                string output = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString() + ".mp4");
                files.Add(output);
                var proc = VideoUtilities.StartFFMpeg("-ss " + (clip.StartTime / 1000f) + " -t " + ((clip.EndTime - clip.StartTime) / 1000f) + " -i \"" + filename + "\" -y -c copy \"" + output + "\"");
                while (!proc.StandardError.EndOfStream)
                {
                    var line = proc.StandardError.ReadLine();
                    Match m = update.Match(line);
                    if (m.Success)
                    {
                        int hours = int.Parse(m.Groups[1].Value);
                        int minutes = int.Parse(m.Groups[2].Value);
                        int seconds = int.Parse(m.Groups[3].Value);
                        int milliseconds = int.Parse(m.Groups[4].Value);
                        long msProcessed = milliseconds + seconds * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
                        data.CurrentValue = totalProcessed + msProcessed;
                        progress.Report(data);
                    }
                }
                totalProcessed += clip.EndTime - clip.StartTime;
                proc.WaitForExit();
            }
            string txtFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString() + ".txt");
            File.WriteAllLines(txtFile, files.Select(x => "file '" + x + "'"));
            var concatProc = VideoUtilities.StartFFMpeg("-f concat -safe 0 -i \"" + txtFile + "\" -y -c copy \"" + exportFile + "\"");
            while (!concatProc.StandardError.EndOfStream)
            {
                var line = concatProc.StandardError.ReadLine();
                Match m = update.Match(line);
                if (m.Success)
                {
                    int hours = int.Parse(m.Groups[1].Value);
                    int minutes = int.Parse(m.Groups[2].Value);
                    int seconds = int.Parse(m.Groups[3].Value);
                    int milliseconds = int.Parse(m.Groups[4].Value);
                    long msProcessed = milliseconds + seconds * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
                    data.CurrentValue = totalProcessed + msProcessed;
                    progress.Report(data);
                }
            }
            concatProc.WaitForExit();
            File.Delete(txtFile);
            foreach (var file in files)
                File.Delete(file);
        }

        public static void WriteTimelineNoTempFiles(Workspace.Workspace workspace, Timeline timeline, string exportFile, IProgress<double> progress)
        {
            List<string> parts = new List<string>();
            List<string> numbers = new List<string>();
            foreach (var clip in timeline.Clips)
            {
                string filename = workspace.Sources.Where(x => x.Name.Equals(clip.SourceName)).Select(x => x.FileName).First();
                string part = "-ss " + (clip.StartTime / 1000f) + " -t " + ((clip.EndTime - clip.StartTime) / 1000f) + " -i \"" + filename + "\"";
                parts.Add(part);
                numbers.Add("[" + (numbers.Count) + "]");
            }
            var command = String.Join(" ", parts) + " -filter_complex \"" + string.Join("", numbers) + "concat=n=" + numbers.Count + ":v=1:a=1\" -c:v libx264 -preset slow -crf 18 -c:a libvorbis -q:a 5 -pix_fmt yuv420p \"" + exportFile + "\"";
            var proc = VideoUtilities.StartFFMpeg(command);
            long msTotal = 0;
            while (!proc.StandardError.EndOfStream)
            {
                var line = proc.StandardError.ReadLine();
                if (msTotal == 0)
                {
                    Match m = duration.Match(line);
                    if (m.Success)
                    {
                        int hours = int.Parse(m.Groups[1].Value);
                        int minutes = int.Parse(m.Groups[2].Value);
                        int seconds = int.Parse(m.Groups[3].Value);
                        int milliseconds = int.Parse(m.Groups[4].Value);
                        msTotal = milliseconds + seconds * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
                    }
                }
                else
                {
                    Match m = update.Match(line);
                    if (m.Success)
                    {
                        int hours = int.Parse(m.Groups[1].Value);
                        int minutes = int.Parse(m.Groups[2].Value);
                        int seconds = int.Parse(m.Groups[3].Value);
                        int milliseconds = int.Parse(m.Groups[4].Value);
                        long msProcessed = milliseconds + seconds * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
                        progress.Report((double)msProcessed / (double)msTotal);
                    }
                }
            }
            proc.WaitForExit();
        }
    }
}
