﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter
{
    public interface IQueueItem
    {
        Action<Task> ContinuationTask { get; set; }
        void Execute();
    }
}
