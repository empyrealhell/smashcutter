﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SmashCutter.Framework
{
    public class ActionCommand : ICommand
    {
        public Action ExecuteAction { get; set; }
        public Func<bool> CanExecuteAction { get; set; }

        public event EventHandler CanExecuteChanged;

        public ActionCommand(Action execute, Func<bool> canExecute, EventHandler executeUpdate)
        {
            this.ExecuteAction = execute;
            this.CanExecuteAction = canExecute;
            executeUpdate += ActionCommand_CanExecuteChanged;
        }

        private void ActionCommand_CanExecuteChanged(object sender, EventArgs e)
        {
            if (this.CanExecuteChanged != null)
                this.CanExecuteChanged(sender, e);
        }

        public bool CanExecute(object parameter)
        {
            return this.CanExecuteAction.Invoke();
        }

        public void Execute(object parameter)
        {
            this.ExecuteAction.Invoke();
        }
    }
}
