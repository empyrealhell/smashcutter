﻿using SmashCutter.ffmpeg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter
{
    public class QueueExport : IQueueItem
    {
        private Workspace.Timeline timeline;
        private Workspace.Workspace workspace;
        private string filename;
        private IProgress<ProgressData> progress;

        public Action<Task> ContinuationTask { get; set; }

        public QueueExport(Workspace.Timeline timeline, Workspace.Workspace workspace, string filename, IProgress<ProgressData> progress)
        {
            this.timeline = timeline;
            this.workspace = workspace;
            this.filename = filename;
            this.progress = progress;
        }

        public void Execute()
        {
            VideoUtilities.WriteTimeline(this.workspace, this.timeline, this.filename, this.progress);
        }
    }
}
