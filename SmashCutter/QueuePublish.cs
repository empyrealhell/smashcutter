﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using SmashCutter.ffmpeg;
using SmashCutter.Youtube;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmashCutter
{
    public class QueuePublish : IQueueItem
    {
        public Action<Task> ContinuationTask { get; set; }

        private Workspace.Workspace workspace;
        private Workspace.Timeline timeline;
        private IProgress<ProgressData> progress;
        private ProgressData data;

        private string categoryId;
        private Playlist playlist;
        private string tempFile;

        public QueuePublish(Workspace.Workspace workspace, Workspace.Timeline timeline, string categoryId, Playlist playlist, IProgress<ProgressData> progress)
        {
            this.workspace = workspace;
            this.timeline = timeline;
            this.progress = progress;
            this.data = new ProgressData();
            this.categoryId = categoryId;
            this.playlist = playlist;
        }

        public void Execute()
        {
            this.tempFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString() + ".mp4");
            VideoUtilities.WriteTimeline(this.workspace, this.timeline, this.tempFile, this.progress);
            FileInfo fi = new FileInfo(this.tempFile);
            this.data.MaxValue = fi.Length;
            this.data.Message = "Uploading";
            try
            {
                var youtubeService = YoutubeUtils.GetService(YoutubeUtils.Authorize());

                Video v = new Video();
                v.Snippet = new VideoSnippet();
                v.Snippet.Title = timeline.Name;
                v.Snippet.CategoryId = this.categoryId;    //pull from api
                v.Status = new VideoStatus();
                v.Status.PrivacyStatus = "public";  //public, private, or unlisted

                using (var stream = new FileStream(this.tempFile, FileMode.Open))
                {
                    var uploadRequest = youtubeService.Videos.Insert(v, "snippet,status", stream, "video/*");
                    uploadRequest.ProgressChanged += UploadRequest_ProgressChanged;
                    uploadRequest.ResponseReceived += UploadRequest_ResponseReceived;
                    uploadRequest.UploadSessionData += UploadRequest_UploadSessionData;
                    var progress = uploadRequest.Upload();
                }
                File.Delete(this.tempFile);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void UploadRequest_UploadSessionData(IUploadSessionData obj)
        {
            //todo:
        }

        private void UploadRequest_ResponseReceived(Video obj)
        {
            if (this.playlist != null)
            {
                var youtubeService = YoutubeUtils.GetService(YoutubeUtils.Authorize());
                YoutubeUtils.AddVideoToPlaylist(youtubeService, this.playlist, obj);
            }
        }

        private void UploadRequest_ProgressChanged(Google.Apis.Upload.IUploadProgress obj)
        {
            switch (obj.Status)
            {
                case UploadStatus.Uploading:
                    this.data.CurrentValue = obj.BytesSent;
                    this.progress.Report(this.data);
                    break;
                case UploadStatus.Failed:
                    Console.WriteLine("Upload failed");
                    break;
            }
        }
    }
}
