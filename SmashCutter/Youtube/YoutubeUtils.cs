﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmashCutter.Youtube
{
    public static class YoutubeUtils
    {
        public static UserCredential Authorize()
        {
            UserCredential cred;
            using (var stream = new FileStream(@"C:\Users\Nick\Downloads\client_id.json", FileMode.Open, FileAccess.Read))
            {
                var secrets = GoogleClientSecrets.Load(stream).Secrets;
                IEnumerable<string> scopes = new string[] { YouTubeService.Scope.Youtube, YouTubeService.Scope.YoutubeUpload };
                Task<UserCredential> task = GoogleWebAuthorizationBroker.AuthorizeAsync(secrets, scopes, "username", CancellationToken.None, new FileDataStore("SmashCutter"));
                try
                {
                    task.RunSynchronously();
                }
                catch (Exception e)
                {

                }
                cred = task.Result;
            }
            return cred;
        }

        public static YouTubeService GetService(UserCredential credentials)
        {
            return new YouTubeService(new BaseClientService.Initializer() { HttpClientInitializer = credentials, ApplicationName = "SmashCutter" });
        }

        public static IEnumerable<Playlist> GetPlaylists(YouTubeService service)
        {
            List<Playlist> playlists = new List<Playlist>();
            PlaylistsResource.ListRequest request = null;
            PlaylistListResponse response = null;
            string pageToken = null;
            do
            {
                request = service.Playlists.List("snippet,status");
                request.Mine = true;
                request.PageToken = pageToken;
                response = request.Execute();
                playlists.AddRange(response.Items);
                pageToken = response.NextPageToken;
            }
            while (pageToken != null);
            return playlists;
        }

        public static void AddVideoToPlaylist(YouTubeService service, Playlist playlist, Video video)
        {
            PlaylistItem item = new PlaylistItem();
            item.Snippet = new PlaylistItemSnippet();
            item.Snippet.ResourceId = new ResourceId();
            item.Snippet.ResourceId.Kind = "youtube#video";
            item.Snippet.ResourceId.VideoId = video.Id;
            item.Snippet.PlaylistId = playlist.Id;
            var request = service.PlaylistItems.Insert(item, "snippet");
            var response = request.Execute();
        }

        public static IEnumerable<VideoCategory> GetCategories(YouTubeService service)
        {
            VideoCategoriesResource.ListRequest request = service.VideoCategories.List("snippet");
            request.RegionCode = "US";
            VideoCategoryListResponse response = request.Execute();
            return response.Items;
        }
    }
}
