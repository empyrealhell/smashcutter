﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter
{
    public class ProgressData
    {
        public string Message { get; set; }
        public long CurrentValue { get; set; }
        public long MaxValue { get; set; }
    }
}
