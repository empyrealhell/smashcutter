﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter.Workspace
{
    public class Timeline
    {
        public string Name { get; set; }
        public IList<Clip> Clips { get; set; }
        [JsonIgnore]
        public long Length
        {
            get
            {
                return this.Clips.Select(x => x.EndTime - x.StartTime).Sum();
            }
        }
        [JsonIgnore]
        public float Progress { get; set; }
        [JsonIgnore]
        public string Message { get; set; }
        [JsonIgnore]
        public bool IsPending { get; set; }
        [JsonIgnore]
        public string SubText
        {
            get
            {
                if (this.IsPending)
                {
                    if (this.Progress > 0)
                    {
                        return this.Message + " - " + this.Progress + "%";
                    }
                    else
                    {
                        return "Pending " + this.Message;
                    }
                }
                else
                {
                    return this.Clips.Count() + " Segments - " + (new TimeSpan(0, 0, 0, 0, (int)this.Length).ToString());
                }
            }
        }

        public Timeline()
        {
            this.Clips = new List<Clip>();
        }

        public Timeline(string Name)
        {
            this.Name = Name;
            this.Clips = new List<Clip>();
        }
    }
}
