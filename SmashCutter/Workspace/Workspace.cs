﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter.Workspace
{
    public class Workspace
    {
        public IList<Timeline> Timelines { get; set; }
        public IList<Source> Sources { get; set; }

        public Workspace()
        {
            this.Timelines = new List<Timeline>();
            this.Sources = new List<Source>();
        }
    }
}
