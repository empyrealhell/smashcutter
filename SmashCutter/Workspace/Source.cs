﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter.Workspace
{
    public class Source
    {
        public string Name { get; set; }
        public string FileName { get; set; }
        public IList<Breakpoint> Breaks { get; set; }
        [JsonIgnore]
        public float Progress { get; set; }
        [JsonIgnore]
        public string Message { get; set; }
        [JsonIgnore]
        public bool IsPending { get; set; }

        public Source()
        {
            this.Breaks = new List<Breakpoint>();
        }

        public Source(string FileName)
        {
            this.Name = FileName;
            this.FileName = FileName;
            this.Breaks = new List<Breakpoint>();
        }

        public Source(string Name, string FileName)
        {
            this.Name = Name;
            this.FileName = FileName;
            this.Breaks = new List<Breakpoint>();
        }
    }
}
