﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter.Workspace
{
    public class Breakpoint
    {
        public long Position { get; set; }
        public bool IsAuto { get; set; }

        public Breakpoint(long position, bool isAuto)
        {
            this.Position = position;
            this.IsAuto = isAuto;
        }
    }
}
