﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter.Workspace
{
    public class Clip
    {
        public string SourceName { get; set; }
        public long StartTime { get; set; }
        public long EndTime { get; set; }

        public Clip(string SourceName, long StartTime, long EndTime)
        {
            this.SourceName = SourceName;
            this.StartTime = StartTime;
            this.EndTime = EndTime;
        }
    }
}
