﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SmashCutter
{
    public static class Utils
    {
        // Import SetThreadExecutionState Win32 API and necessary flags
        [DllImport("kernel32.dll")]
        private static extern uint SetThreadExecutionState(uint esFlags);
        private const uint ES_CONTINUOUS = 0x80000000;
        private const uint ES_SYSTEM_REQUIRED = 0x00000001;

        private static uint previousState = 0;
        public static void SetThreadContinuous()
        {
            if (previousState == 0)
            {
                uint current = SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED);
                if (current != 0)
                {
                    previousState = current;
                }
            }
        }

        public static void SetThreadNormal()
        {
            if (previousState != 0)
            {
                uint current = SetThreadExecutionState(previousState);
                if (current != 0)
                {
                    previousState = 0;
                }
            }
        }
    }
}
